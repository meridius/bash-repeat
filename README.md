# Repeat
*Run command on changes in monitored files and directories.*

Bash wrapper for [Entr](http://entrproject.org/) tool.

## Usage

```
repeat <options> <command>
```

### Command options

- `-w` or `--watch`
  - Define what files/directories to watch. Can be specified multiple times.
- `-g` or `--glob`
  - Filter watched files by glob pattern. Don't forget escaping (\*).
- `-h` or `--help`
  - Display this help and exit.
