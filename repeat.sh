#!/usr/bin/env bash

set -eu

readonly R_YES=0
readonly R_NO=1
WATCHES=()
GLOBS=()
CLEAR_OUTPUT="${R_YES}"

app_help() {
read -r -d '\0' USAGE <<EOF
-= Repeat =-
Wrapper for Entr tool.
Run command on changes in monitored files and directories.

Usage:
    repeat <options> <command>

Command options:
    -w|--watch            Define what files/directories to watch. Can be specified multiple times.
    -g|--glob             Filter watched files by glob pattern. Don't forget escaping (\*).
    -n|--no-clear         Do not clear previous output before running next iteration.
    -h|--help             Display this help and exit.
\0
EOF

    echo "${USAGE}"
}

app_run() {
    local glob_flags=()
    local entr_flags=("-s")
    local task

    while [[ $# -gt 0 ]]; do
        case "$1" in
            -w|--watch)    WATCHES+=("$2") ;        shift 2 ;;
            -g|--glob)     GLOBS+=("$2") ;          shift 2 ;;
            -n|--no-clear) CLEAR_OUTPUT="${R_NO}" ; shift   ;;
            -h|--help)     app_help ;               exit 0  ;;
            *)             task="$@" ;              break   ;;
        esac
    done

    if [[ "${CLEAR_OUTPUT}" -eq "${R_YES}" ]]; then
        entr_flags+=("-c")
    fi

    for glob in "${GLOBS[@]}"; do
        iname_flags=("-iname" "'${glob}'")

        if [[ "${#glob_flags[@]}" -eq 0 ]]; then
            glob_flags+=("${iname_flags[@]}")
        else
            glob_flags+=("${glob_flags[@]}" "-or" "${iname_flags[@]}")
        fi
    done

    cmd_find=( find "${WATCHES[@]}" "${glob_flags[@]}" )
    cmd_entr=( entr "${entr_flags[@]}" "${task[@]}" )

    while true; do
        if "${cmd_find[@]}" | "${cmd_entr[@]}"; then
            break
        else
            echo "${cmd_find[@]} | ${cmd_entr[@]}"
        fi
    done
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    app_run "$@"
fi
